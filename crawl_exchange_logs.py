import json
import requests
from eth_utils import remove_0x_prefix, add_0x_prefix

def pad_hex(value, size):
    """
    Pads a hex string up to the given size
    """
    value = remove_0x_prefix(value)

    return add_0x_prefix(value.zfill(int(size)))

def get_logs(blockNumber, address, infura_project_id, event_filter, address_filter):

    if address_filter is None:
        params = [
            {
                "fromBlock": hex(blockNumber),
                "address" : address,
                "topics":[event_filter]
            }
        ]
    else:
        params = [
            {
                "fromBlock": hex(blockNumber),
                "address" : address,
                "topics":[event_filter, pad_hex(address_filter, 64)]
            }
        ]

    # setup the payload
    payload = {
        "jsonrpc" : "2.0",
        "method" : "eth_getLogs",
        "params" : params,
        "id" : 1
    }

    # setup the headers
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    url = "https://mainnet.infura.io/v3/" + infura_project_id;

    # make the request
    r = requests.post(url, data=json.dumps(payload), headers=headers);
    return r
