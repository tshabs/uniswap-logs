README
======

Uses infura API to get uniswap exchange logs.

Setup
-----
```
pip install -r requirements.txt
```

Run
---
```
python uniswap_exchanges.py infura_project_id blockNumber address_filter(optional)
```
