from web3.auto.infura.mainnet import w3
from web3.middleware import geth_poa_middleware
from web3 import Web3
import os
import sys
from crawl_exchange_logs import get_logs, pad_hex
from libjson2csv.json_2_csv import convert_to_csv


if (len(sys.argv) < 3):
    print("Expected at least 2 args! [infura_project_id, blockNumber]");
    print("Usage: ./uniswap_exchanges.py [infura_project_id, blockNumber, address_filter(optional)]");
    sys.exit()

# delete previous logs if found
fname = 'logs.csv'
if (os.path.exists(fname)):
    os.remove(fname);

# grab the input parameters
infura_project_id = sys.argv[1];
blockNumber = int(sys.argv[2]);
try:
    address_filter = sys.argv[3];
except:
    address_filter = None

token_purchase_event = pad_hex(Web3.keccak(text="TokenPurchase(address,uint256,uint256)").hex(), 64)
eth_purchase_event = pad_hex(Web3.keccak(text="EthPurchase(address,uint256,uint256)").hex(), 64)

NETWORK = "mainnet"

UNISWAP_FACTORY_ADDRESS = {
    "mainnet": "0xc0a47dFe034B400B47bDaD5FecDa2621de6c4d95",
#    "rinkeby": "0xf5D915570BC477f9B8D6C0E980aA81757A3AaC36"
}

UNISWAP_FACTORY_ABI = """[{"name": "NewExchange", "inputs": [{"type": "address", "name": "token", "indexed": true}, {"type": "address", "name": "exchange", "indexed": true}], "anonymous": false, "type": "event"}, {"name": "initializeFactory", "outputs": [], "inputs": [{"type": "address", "name": "template"}], "constant": false, "payable": false, "type": "function", "gas": 35725}, {"name": "createExchange", "outputs": [{"type": "address", "name": "out"}], "inputs": [{"type": "address", "name": "token"}], "constant": false, "payable": false, "type": "function", "gas": 187911}, {"name": "getExchange", "outputs": [{"type": "address", "name": "out"}], "inputs": [{"type": "address", "name": "token"}], "constant": true, "payable": false, "type": "function", "gas": 715}, {"name": "getToken", "outputs": [{"type": "address", "name": "out"}], "inputs": [{"type": "address", "name": "exchange"}], "constant": true, "payable": false, "type": "function", "gas": 745}, {"name": "getTokenWithId", "outputs": [{"type": "address", "name": "out"}], "inputs": [{"type": "uint256", "name": "token_id"}], "constant": true, "payable": false, "type": "function", "gas": 736}, {"name": "exchangeTemplate", "outputs": [{"type": "address", "name": "out"}], "inputs": [], "constant": true, "payable": false, "type": "function", "gas": 633}, {"name": "tokenCount", "outputs": [{"type": "uint256", "name": "out"}], "inputs": [], "constant": true, "payable": false, "type": "function", "gas": 663}]"""

UNISWAP_FACTORY_CONTRACT = w3.eth.contract(address=UNISWAP_FACTORY_ADDRESS[NETWORK], abi=UNISWAP_FACTORY_ABI)

token_count = UNISWAP_FACTORY_CONTRACT.functions.tokenCount().call()
print('Token Count: {}'.format(token_count))
n = 0

while(n <= token_count):
    print('=========================')
    token = UNISWAP_FACTORY_CONTRACT.functions.getTokenWithId(n).call()
    print('Token Contract Address: {}'.format(token))
    exchange = UNISWAP_FACTORY_CONTRACT.functions.getExchange(token).call()
    print('Uniswap Exchange Contract Address: {}'.format(exchange))

    r = get_logs(blockNumber, exchange, infura_project_id, token_purchase_event, address_filter)

    with open('logs.csv', 'a') as csvfile:
        convert_to_csv(r.json()["result"], csvfile)

    r = get_logs(blockNumber, exchange, infura_project_id, eth_purchase_event, address_filter)

    with open('logs.csv', 'a') as csvfile:
        convert_to_csv(r.json()["result"], csvfile)

    n += 1
